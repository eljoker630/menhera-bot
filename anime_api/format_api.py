import emoji


def get_five_stars(points=0.0):
    return ''.join(emoji.emojize(':star:') for a in range(round(points)))


def reformat_chapter(chapter: dict, selectable):
    next_chapter = f'\nProximo Episodio: :calendar: {chapter["next-chapter"]}'
    next_chapter = '' if chapter['next-chapter'] is None else next_chapter
    synopsis = chapter["synopsis"] if chapter["synopsis"] is not None else ''
    text = [
        f'{chapter["name"]}',
        f'Estado: {chapter["status"]}{next_chapter}',
        f'Categoias: {chapter["category-list"]}',
        f'Votos: {get_five_stars(chapter["votes"])}',
        f'Sinopsis: {synopsis[:240 - 3]}...',
    ]
    if selectable:
        command = '\nSelecciona el capitulo usando el'
        command += f' comando /chapter_{chapter["id"]}'
        text.append(command)
    return emoji.emojize('\n'.join(data for data in text))


def reformat_anime(anime: dict, selectable):
    next_chapter = f'\nProximo Episodio: :calendar: {anime["next-chapter"]}'
    next_chapter = '' if anime['next-chapter'] is None else next_chapter
    synopsis = anime["synopsis"] if anime["synopsis"] is not None else ''
    if len(anime["related"]) != 0:
        related_anime = '\n'.join(related for related in anime["related"])
    else:
        related_anime = 'No hay ningun anime relacionado.'
    text = [
        f'{anime["name"]}',
        f'Estado: :warning:{anime["status"]}{next_chapter}',
        f'Categoias: {anime["category-list"]}',
        f'Votos: {get_five_stars(anime["votes"])}',
        f'Capitulos: {len(anime["chapters"])}',
        f'Sinopsis: {synopsis[:240 - 3]}...',
        f'\nAnimes relacionados:\n{related_anime}'
    ]
    if selectable:
        command = '\nPara seleccionar este anime utiliza'
        command += f' el comando /anime_{anime["id"]}'
        text.append(command)
    return emoji.emojize('\n'.join(data for data in text))
