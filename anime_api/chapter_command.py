import os

import mega
import telebot
from requests import Session
from telegram import Message

from anime_api.base_api import AnimeDetails
from anime_api.serializer import reformat_chapters, keyboard_chapter
from cloud_manager.zippyshare_api import Zippy
from utils.bot_logger import BotLogger
from utils.file_manager import UploadManager
from utils.sticker_manager import send_sticker


class DownloadMethods:
    def __init__(self, upload_manager: UploadManager):
        self.manager = upload_manager

    def mega_progress(self, message: Message, url, path):
        mega.mega.requests = Session()
        mega.mega.requests.proxies = self.manager.proxy
        mega_dl = mega.mega.Mega().download_url
        file_name = str(mega_dl(url, dest_path=path))
        self.manager.upload_file(message, file_name)

    def zippy_progress(self, message: Message, url, path):
        zippy = Zippy(url)
        zippy.request.proxies = self.manager.proxy
        zippy.start()
        file_name = zippy.download(path).name
        self.manager.upload_file(message, file_name)


class ChapterCommands:
    def __init__(self, bot: telebot.TeleBot, lang: dict, anime_api: AnimeDetails):
        self.bot, self.lang = bot, lang
        self.anime_api = anime_api
        self.chapter_list = []

    def start_command(self, message: Message):
        next_step = self.bot.register_next_step_handler
        send_sticker(self.bot, message.chat.id, "OK!")
        main_page = self.anime_api.main_page()
        self.chapter_list = main_page.chapter_list
        reply = self.print_chapters(message)
        next_step(reply, self.select_chapter)

    def print_chapters(self, message: Message, show_list=True):
        send_message = self.bot.send_message
        chat_id = message.chat.id
        serializer = reformat_chapters(self.chapter_list, self.lang)
        chapter_length = range(len(self.chapter_list))
        chapter_length = [('%02d' % (i + 1)) for i in chapter_length]
        markup = keyboard_chapter(chapter_length, ['Cancelar'])
        text = f"{serializer['text']}\n{serializer['action-text']}"
        if not show_list: text = serializer['action-text']
        return send_message(chat_id, text, reply_markup=markup)

    def select_chapter(self, message: Message):
        if 'cancelar' in message.text.lower():
            markup = telebot.types.ReplyKeyboardRemove()
            args = [self.bot, message.chat.id, 'OK!']
            send_sticker(*args, reply_markup=markup)
            return 0
        try:
            chapter_id = int(message.text) - 1
            chapter = self.chapter_list[chapter_id]['url']
            chapter = self.anime_api.chapter_page(chapter)
            self.select_download(message.chat.id, chapter)
        except: BotLogger().error_log()

    def select_download(self, chat_id: int, chapter: dict):
        args = {'chat_id': chat_id, 'text': self.lang["select-cloud"]}
        kwargs = {'resize_keyboard': True, 'one_time_keyboard': True}
        args['reply_markup'] = telebot.types.ReplyKeyboardMarkup(**kwargs)

        button = telebot.types.KeyboardButton
        buttons = [button(url['server']) for url in chapter['urls']]
        args['reply_markup'].add(*buttons, button('Cancelar'))
        args = [self.bot.send_message(**args), self.download_chapter]
        self.bot.register_next_step_handler(*args, chapter=chapter)

    def download_chapter(self, message: Message, chapter: dict):
        path = f'downloads{os.sep}{message.from_user.id}{os.sep}'
        os.makedirs(path) if not os.path.isdir(path) else None
        markup = telebot.types.ReplyKeyboardRemove()
        chat_id = message.chat.id
        send_sticker(self.bot, chat_id, "OK!", reply_markup=markup)
        if 'cancelar' in message.text.lower(): return 0
        cloud = next((d for d in chapter['urls'] if d['server'] ==
                      message.text), {'server': None, 'url': None})
        if cloud['server'] is None:
            send_sticker(self.bot, chat_id, "Question", chat_id)
            command = {'command': message.text}
            text = self.lang["command-notfound"].format(**command)
            return self.bot.send_message(chat_id, text)
        try:
            text_format = {'name': chapter['name'], 'size': cloud['server']}
            text = self.lang["start-download"].format(**text_format)
            markup = telebot.types.ReplyKeyboardRemove()
            self.bot.send_message(chat_id, text, reply_markup=markup)

            download_manager = UploadManager(self.bot, self.lang)
            download_manager.proxy = self.anime_api.cloud_scraper.proxies
            download_method = DownloadMethods(download_manager)
            if cloud['server'] == 'Stape':
                send_sticker(self.bot, chat_id, "Sorry")
                text = self.lang["method-not-allowed"]
                self.bot.send_message(chat_id, text)
                self.select_download(chat_id, chapter)
                return

            method = download_method.zippy_progress
            if cloud['server'] == 'MEGA':
                method = download_method.mega_progress
            method(message, cloud['url'], path)
            next_step = self.bot.register_next_step_handler
            reply = self.print_chapters(message, False)
            next_step(reply, self.select_chapter)
        except Exception as error:
            BotLogger().error_log()
            send_sticker(self.bot, chat_id, '')
            self.bot.send_message(chat_id, str(error))
