import emoji
import telebot


def get_five_stars(points=0.0):
    star = emoji.emojize(':star:')
    points = range(round(points))
    return ''.join(star for a in points)


def keyboard_chapter(chapter_list, commands=None):
    markup = telebot.types.ReplyKeyboardMarkup()
    markup.one_time_keyboard = True
    markup.resize_keyboard = True
    keyboard_button = telebot.types.KeyboardButton
    for index, chapter in enumerate(chapter_list):
        column = index * 3
        length = chapter_list[column:column + 3]
        markup.add(*[keyboard_button(i) for i in length])
    if commands is not None: markup.add(*[
        keyboard_button(command)
        for command in commands
    ])
    return markup


def reformat_anime(anime: dict, lang: dict):
    next_chapter = lang["next-chapter"].format(date=anime["next-chapter"])
    next_chapter = '' if anime['next-chapter'] is None else next_chapter
    synopsis = anime["synopsis"] if anime["synopsis"] is not None else ''
    related_anime = '\n'.join(related for related in anime["related"])
    if len(anime["related"]) != 0: related_anime = lang["no-related"]
    format_text = {'object': 'Anime', 'command': f'anime_{anime["id"]}'}
    return {
        'text': emoji.emojize('\n'.join(data for data in [
            f'{anime["name"]}',
            f'Estado: :warning:{anime["status"]}{next_chapter}',
            f'Categoias: {anime["category-list"]}',
            f'Votos: {get_five_stars(anime["votes"])}',
            f'Capitulos: {len(anime["chapters"])}',
            f'Sinopsis: {synopsis[:240 - 3]}...',
            f'\nAnimes relacionados:\n{related_anime}'
        ])),
        'action-text': lang["select-object"].format(**format_text),
        **anime
    }


def reformat_chapters(chapters: list, lang: dict):
    text = lang["listing-chapters"]
    for index, chapter in enumerate(chapters):
        id = '\n[%02d]' % (index + 1)
        text += f"{id} » {chapter['title']} "
        text += chapter['chapter']
    return {
        'text': text, 'chapter-list': chapters,
        'action-text': lang["select-chapter"]
    }
