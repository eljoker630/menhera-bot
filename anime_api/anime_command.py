import telebot

from anime_api.base_api import AnimeDetails
from anime_api.chapter_command import ChapterCommands
from anime_api.format_api import reformat_anime
from utils.sticker_manager import send_sticker


class AnimeCommands:
    def __init__(self, bot: telebot.TeleBot, lang: dict, anime_api: AnimeDetails):
        self.bot, self.lang = bot, lang
        self.anime_api = anime_api
        args = (self.bot, self.lang, self.anime_api)
        self.command_manager = ChapterCommands(*args)
        self.anime_list = []

    def print_anime(self, anime: dict, chat_id, selectable=True):
        image = anime['image']
        message = reformat_anime(anime, selectable)
        return self.bot.send_photo(chat_id, image, message)

    def anime_today(self, message):
        send_sticker(self.bot, message.chat.id, 'Sir, Yes, Sir')
        kwargs = (self.print_anime, {'chat_id': message.chat.id})
        self.anime_list = self.anime_api.anime_today(*kwargs)
        text = self.lang["specific-chapter"]
        reply = self.bot.send_message(message.chat.id, text)
        self.bot.register_next_step_handler(reply, self.anime_operation)

    def anime_search(self, message):
        anime_name = message.json['text'].replace('/anime_search ', '')
        chat_id = message.chat.id
        if len(anime_name) == 0:
            text = self.lang["not-field-search"]
            send_sticker(self.bot, chat_id, "Question")
            return self.bot.send_message(chat_id, text)
        results = self.anime_api.find_anime(anime_name)
        for anime_url in results:
            anime = self.anime_api.anime_page(anime_url)
            self.anime_list.append(anime)
            self.print_anime(anime, message.chat.id)
        text = self.lang["end-anime-search"]
        text = text.format(anime=anime_name)
        reply = self.bot.send_message(chat_id, text)
        self.bot.register_next_step_handler(reply, self.anime_operation)

    def anime_operation(self, message):
        if message.text == '/cancel':
            return 0
        chat_id = message.chat.id
        anime_id = int(message.text.replace('/anime_', ''))
        anime = next((a for a in self.anime_list if a['id'] == anime_id))
        self.print_anime(anime, chat_id, False)
        self.command_manager.chapter_list = [{
            'image': anime['image'],
            'title': anime['name'],
            'chapter': 'Episodio ' + url.split('-')[-1],
            'url': url
        } for url in anime['chapters']]
        reply = self.command_manager.print_chapters(message, False)
        callback = self.command_manager.select_chapter
        self.bot.register_next_step_handler(reply, callback)
