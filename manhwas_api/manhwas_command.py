import os
import shutil

import telebot
from telegram import Message

from manhwas_api.tumanhwas_api import TuManhwas
from split_compress import zip_file
from utils.bot_logger import BotLogger
from utils.file_manager import UploadManager
from utils.sticker_manager import send_sticker


class ManhwaCommands:
    def __init__(self, bot: telebot.TeleBot, lang: dict):
        self.manhwa_api = TuManhwas()
        self.manhwa_List = []
        self.chapter_list = []
        self.bot, self.lang = bot, lang

    def print_chapters(self, message: Message):
        text = 'Lista de los ultimos capitulos agregados \n'
        markup = telebot.types.ReplyKeyboardMarkup
        markup = markup(one_time_keyboard=True, resize_keyboard=True)
        button = telebot.types.KeyboardButton
        chat_id = message.chat.id
        send_message = self.bot.send_message
        length = range(len(self.manhwa_List))
        for index, chapter in enumerate(self.manhwa_List):
            col = index * 3
            markup.add(*[button('%02d' % (i + 1))
                         for i in length[col:col + 3]])
            chapter_number = chapter["url"].split('-')[-1]
            number = '%02d' % (index + 1)
            text += f'[{number}] {chapter["name"]}'
            text += f' capitulo {chapter_number}\n'
        text += 'Utiliza el comando /cancel si no desea nada'
        return send_message(chat_id, text, reply_markup=markup)

    def last_chapter(self, message: Message):
        self.manhwa_List = self.manhwa_api.get_list(box_id=0)
        self.manhwa_List = self.manhwa_List['manhwas']
        reply = self.print_chapters(message)
        callback = self.download_chapter
        self.bot.register_next_step_handler(reply, callback)

    def download_chapter(self, message: Message):
        if '/cancel' == message.text:
            markup = telebot.types.ReplyKeyboardRemove()
            args = self.bot, message.chat.id, "Roger"
            return send_sticker(*args, reply_markup=markup)
        try:
            chapter_id = int(message.text) - 1
            account_id = message.from_user.id
            chapter = self.manhwa_List[chapter_id]
            zipped = self.zip_chapter(chapter, account_id)
            manager = UploadManager(self.bot, self.lang)
            manager.proxy = self.manhwa_api.request.proxies
            manager.upload_file(message, zipped.name)
        except: pass

    def print_manhwa(self, chat_id, manhwa, selectable=True):
        manhwa_id = self.manhwa_List.index(manhwa)
        details = self.manhwa_api.get_manhwa(manhwa['url'])
        details['url'] = manhwa['url']
        category = ', '.join(c for c in details['category'])
        text = [
            f"{details['title']}",
            f"Vistas: {details['views']}",
            f"Categorias: {category}",
            f"Capitulos: {len(details['chapter-list'])}",
            f"Sinopsis: {details['synopsis'][:190]}...",
        ]
        if selectable:
            command = "\n Para descargar este manhwa utilice el"
            text.append(f"{command} comando /mahwa_{manhwa_id}")
        text = '\n'.join(line for line in text)
        self.bot.send_photo(chat_id, details['image'], text)
        return details

    def latest_manhwa(self, message: Message):
        chat_id = message.chat.id
        self.manhwa_List = self.manhwa_api.get_list()
        self.manhwa_List = self.manhwa_List['manhwas']
        self.manhwa_List = [self.print_manhwa(chat_id, manhwa)
                            for manhwa in self.manhwa_List]
        text = "task-terminated", "allow-cancel"
        text = ' '.join(self.lang[t] for t in text)
        reply = self.bot.send_message(chat_id, text)
        callback = self.select_manhwa
        self.bot.register_next_step_handler(reply, callback)

    def find_manhwa(self, message: Message):
        chat_id = message.chat.id
        search = message.text.replace('/find_manhwa ', '')
        find_manhwa = self.manhwa_api.find_manhwa
        next_page = 1
        while next_page is not None:
            result = find_manhwa(search, next_page)
            details = []
            for manhwa in result['manhwas']:
                self.manhwa_List.append(manhwa)
                details.append(self.print_manhwa(chat_id, manhwa))
            self.manhwa_List += details
            next_page = result['paginator']['next-page']
        text = self.lang["task-terminated"]
        text += self.lang["allow-cancel"]
        reply = self.bot.send_message(chat_id, text)
        callback = self.select_manhwa
        self.bot.register_next_step_handler(reply, callback)

    def select_manhwa(self, message: Message):
        chat_id = message.chat.id
        if 'cancel' in message.text.lower():
            return send_sticker(self.bot, chat_id, 'Good')
        manhwa_id = int(message.text.replace('/mahwa_', ''))
        manhwa = self.manhwa_List[manhwa_id]
        choices = [chapter['name'] for chapter in manhwa['chapter-list']]
        markup = telebot.types.ReplyKeyboardMarkup
        markup = markup(one_time_keyboard=True, resize_keyboard=True)
        button = telebot.types.KeyboardButton
        markup.add(*[button('Todas'), button('Cancelar')])
        for index, choice in enumerate(choices):
            col = index * 3
            markup.add(*[button(c) for c in choices[col:col + 3]])
        self.print_manhwa(chat_id, manhwa, False)
        text = 'Que capitulos vas a descargar?'
        reply = self.bot.send_message(chat_id, text, reply_markup=markup)
        callback = self.select_chapter
        next_step = self.bot.register_next_step_handler
        next_step(reply, callback, manhwa=manhwa)

    def select_chapter(self, message: Message, manhwa):
        chapters = manhwa['chapter-list']
        chapters = [chapter['name'] for chapter in chapters]
        account_id = message.from_user.id
        chat_id = message.chat.id
        markup = telebot.types.ReplyKeyboardRemove()
        if message.text == 'Cancelar':
            send_sticker(self.bot, chat_id, "OK!", reply_markup=markup)
            return 0
        elif message.text == 'Todas':
            chapters = [
                self.zip_chapter(manhwa['chapter-list'][i], account_id).name
                for i, c in enumerate(chapters)]
            down = f"downloads{os.sep}{account_id}{os.sep}"
            name = f"{down}{manhwa['url'].split('/')[-1]}.zip"
            zipped = zip_file(chapters, name)
            [os.remove(chapter) for chapter in chapters]
        else:
            chapter = chapters.index(message.text)
            chapter = manhwa['chapter-list'][chapter]
            zipped = self.zip_chapter(chapter, account_id)
        send_sticker(self.bot, chat_id, "Roger", reply_markup=markup)
        manager = UploadManager(self.bot, self.lang)
        manager.proxy = self.manhwa_api.request.proxies
        manager.upload_file(message, zipped.name)

    def zip_chapter(self, chapter, account_id):
        down = f"downloads{os.sep}{account_id}{os.sep}"
        path = f"{down}{chapter['url'].split('/')[-1]}"
        os.makedirs(path) if not os.path.isdir(path) else None
        get_images = self.manhwa_api.get_images
        images = get_images(chapter['url'], path)
        zipped = zip_file(images, f'{path}.zip')
        shutil.rmtree(path)
        return zipped

    def download_manhwa(self, message: Message):
        try:
            manhwa_id = int(message.text.replace('/mahwa_', ''))
            manhwa = self.manhwa_List[manhwa_id]
            send_sticker(self.bot, message.chat.id, "Roger")
            path = f'downloads{os.sep}{message.chat.id}'
            compress_file = self.manhwa_api.download
            zip_file = compress_file(manhwa['url'], path)
            manager = UploadManager(self.bot, self.lang)
            manager.proxy = self.manhwa_api.request.proxies
            manager.upload_file(message, zip_file.name)
        except:
            BotLogger().error_log()
            text = 'Operacion cancelada'
            self.bot.send_message(message.chat.id, text)
