import os
import re
import shutil

import requests
from bs4 import BeautifulSoup


class TuManhwas:
    def __init__(self):
        self.base_url = 'https://tumanhwas.com/'
        self.request = requests.Session()

    def paginator(self, soup):
        paginator = soup.find('ul', {'class': 'pagination'})
        if paginator is None:
            return {'active': None, 'next-page': None}
        li_active = {'class': 'page-item active'}
        active = int(paginator.find('li', li_active).text)
        last_item = paginator.find_all('li')[-1]
        next_page = last_item.find('a')
        if next_page is not None:
            next_link = last_item.find('a')['href']
            index = next_link.index('page') + 5
            next_page = int(next_link[index:])
        return {'active': active, 'next-page': next_page}

    def get_list(self, url=None, box_id=-1) -> dict:
        url = self.base_url if url is None else url
        request = self.request.get(url).content
        soup = BeautifulSoup(request, 'html.parser')
        box = soup.find_all('div', {'class': 'listupd'})
        return {
            'paginator': self.paginator(soup),
            'manhwas': [{
                'image': chapter.find('img')['src'],
                'url': chapter['href'],
                'name': chapter.find('div', {'class': 'tt'}).string
            } for chapter in box[box_id].find_all('a', {'class': ''})]
        }

    def find_manhwa(self, name, page=1):
        args = {'search': name, 'page': page}
        args = '&'.join(f'{arg}={args[arg]}' for arg in args)
        url = f'{self.base_url}biblioteca?{args}'
        return self.get_list(url)

    def get_genre(self):
        request = self.request.get(self.base_url).content
        soup = BeautifulSoup(request, 'html.parser')
        genre_list = soup.find('ul', {'class': 'genre'})
        genre_list = genre_list.find_all('a')
        return [{'name': a.text, 'url': a['href']} for a in genre_list]

    def get_manhwa(self, url):
        request = self.request.get(url).text
        soup = BeautifulSoup(request, 'lxml')
        chapter_list = soup.find('div', {'id': 'chapterlist'})
        chapter_list = chapter_list.find_all('a')
        synopsis = soup.find('div', {'itemprop': 'description'})
        chapter_list = [{
            'url': chapter['href'],
            'name': chapter['href'].split('-')[-1]
        } for chapter in chapter_list]
        return {
            'title': soup.find('h1', {'class': 'entry-title'}).string,
            'views': int(soup.find_all('div', {'class': 'bookmark'})[1].text),
            'image': soup.find('img', {'itemprop': 'image'})['src'],
            'category': [a.string for a in soup.find_all('a', {'rel': 'tag'})],
            'synopsis': synopsis.text.replace('\n', ''),
            'chapter-list': chapter_list,
        }

    def get_chapter(self, url):
        request = self.request.get(url).content
        soup = BeautifulSoup(request, 'html.parser')
        images = soup.find('div', {'id': 'chapter_imgs'})
        return [image['src'] for image in images.find_all('img')]

    def get_images(self, chapter_url, path):
        images = self.get_chapter(chapter_url)
        data = []
        for number, image in enumerate(images):
            image_format = image.split('.')[-1]
            image_name = f'{number}.{image_format}'
            image_path = os.path.join(path, image_name)
            file = open(image_path, 'wb')
            file.write(self.request.get(image).content)
            data.append(file.name)
        return data

    def download(self, url, path):
        is_dir = os.path.isdir
        path = path + os.sep if path[-1] != os.sep else path
        path = path + url.split('/')[-1] + os.sep
        os.makedirs(path) if not is_dir(path) else None

        # List all chapters and create chapter dir
        for chapter in self.get_manhwa(url)['chapter-list']:
            chapter_path = re.search(r'(?P<number>[0-9.*]+)', chapter["name"])
            chapter_path = path + chapter_path.group() + os.sep
            os.makedirs(chapter_path) if not is_dir(chapter_path) else None
            self.get_images(chapter['url'], chapter_path)
        zip_file = os.path.join(path, url.split('/')[-1])
        shutil.make_archive(zip_file, 'zip', root_dir=path)
        shutil.rmtree(path)
        return open(zip_file + '.zip', 'rb')
