import json
import os

current_path = os.path.dirname(__file__) + os.sep
stickers = open(current_path + 'sticker_list.json', 'r')
sticker_list = json.loads(stickers.read())


def send_sticker(bot, chat_id, sticker_name, reply_to=None, reply_markup=None):
    default_sticker = "CAACAgIAAxkBAAIIBWL9oLQirzYbbhd3JXSpvQVwacCf"
    default_sticker += "AALbPwAC4KOCB8ZOBCb-SNAGKQQ"
    sticker = next((
        sticker["sticker"] for sticker in sticker_list
        if sticker["name"] == sticker_name), default_sticker
    )
    more_args = {'reply_to_message_id': reply_to, 'reply_markup': reply_markup}
    return bot.send_sticker(chat_id, sticker, **more_args)


def add_sticker(sticker_name, sticker_id):
    file = open('stickers.name', 'wb')
    data = {'name': sticker_name, 'sticker': sticker_id}
    sticker_list.append(data)
    file.write(sticker_list)
    file.close()
    return sticker_list
