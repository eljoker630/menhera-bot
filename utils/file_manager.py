import json
import os

import mega
import requests
import telebot
from requests import Session
from telegram import Message

from cloud_manager.moodle_api import MoodleScraper
from cloud_manager.moodle_command import MoodleCommand
from cloud_manager.one_fichier_api import OneFichier
from cloud_manager.zippyshare_api import Zippy
from split_compress import file_split, zip_file
from utils.sticker_manager import send_sticker


class UploadManager:
    def __init__(self, bot: telebot.TeleBot, lang: dict):
        self.max_size = 49
        self.bot = bot
        self.lang = lang
        self.proxy = {}

    def check_size(self, file_name):
        file_size = os.path.getsize(file_name)
        if int(file_size / 1024 ** 2) < self.max_size:
            return [open(file_name, 'rb')]
        zipped = zip_file([file_name], file_name + '.zip')
        os.remove(zipped.name)
        os.remove(file_name)
        path = file_name.replace(file_name.split(os.sep)[-1], '')
        return file_split(zipped, self.max_size, path)

    def wget_upload(self, message: Message):
        try:
            url = message.text.replace('/wget ', '')
            account_id = message.from_user.id

            request = requests.Session()
            request.proxies = self.proxy
            content = request.get(url).content

            path = f'downloads{os.sep}{account_id}{os.sep}'
            file = open(path + url.split('/')[-1], 'wb')
            file.write(content)
            file.close()
            self.upload_file(message, str(file.name))
        except Exception as error:
            chat_id = message.chat.id
            send_sticker(self.bot, chat_id, '')
            self.bot.send_message(chat_id, str(error))

    def zippy_upload(self, message: Message):
        try:
            url = message.text.replace('/zippy ', '')
            zippy = Zippy(url)
            zippy.request.proxies = self.proxy
            zippy.start()
            path = os.path.join('downloads', str(message.from_user.id))
            file = zippy.download(path)
            self.upload_file(message, str(file.name))
        except Exception as error:
            chat_id = message.chat.id
            send_sticker(self.bot, chat_id, '')
            self.bot.send_message(chat_id, str(error))

    def mega_upload(self, message: Message):
        path = f'downloads/{message.from_user.id}'
        file_url = message.text.replace('/mega ', '')

        mega.mega.requests = Session()
        proxies = self.proxy
        mega.mega.requests.proxies = proxies
        mega_dl = mega.mega.Mega().download_url
        file_name = str(mega_dl(file_url, dest_path=path))
        self.upload_file(message, file_name)

    def one_fichier_upload(self, message: Message):
        file_url = message.text.replace('/1fichier ', '')
        one_fichier = OneFichier(file_url)
        one_fichier.request.proxies = self.proxy
        details = one_fichier.get_details()
        path = f'downloads/{message.from_user.id}{os.sep}'
        path += details['file-name']
        if 'cooldown' in details:
            send_sticker(self.bot, message.chat.id, "")
            text = 'One Fichier nos ha bloqueado, espera'
            text += f' {details["cooldown"]} minutos.'
            self.bot.send_message(message.chat.id, text)
            return 0
        file = one_fichier.request.get(details['url-file'])
        open(path, 'wb').write(file.content)
        zipped = zip_file([path], path + '.zip')
        os.remove(path)
        self.upload_file(message, zipped.name)

    def upload_file(self, message: Message, file_path: str):
        chat_id = message.chat.id
        user_id = message.from_user.id
        send_document = self.bot.send_document
        send_message = self.bot.send_message
        text = self.lang["file-downloaded"]
        reply = send_message(chat_id, text)
        moodle_manager = MoodleCommand(self.bot, self.lang)
        moodle_cloud = moodle_manager.get_moodle(user_id)
        selected = moodle_cloud.selected if moodle_cloud else False
        args = {'timeout': 60 ** 3}
        if not selected:
            for file in self.check_size(file_path):
                send_document(chat_id, file, **args)
                os.remove(file.name)
            return 0

        moodle_api = MoodleScraper(moodle_cloud.url)
        moodle_api.response.proxies = self.proxy
        data = {
            'credentials': {
                'username': moodle_cloud.username,
                'password': moodle_cloud.password,
            },
            'url-files': []
        }
        moodle_api.authenticate(**data['credentials'])
        moodle_api.credentials['repo-id'] = moodle_cloud.repo_id
        if 'session-key' not in moodle_api.credentials:
            text = 'Credenciales incorrectas'
            chat_id = message.chat.id
            self.bot.send_message(chat_id, text)
            return 1

        for file in self.check_size(file_path):
            file_name = file.name.split(os.sep)[-1]
            text = self.lang["moodle-uploading"]
            text = text.format(archive=file_name)
            self.bot.edit_message_text(text, chat_id, reply.id)
            moodle_link = moodle_api.upload_file(file)
            data['url-files'].append(moodle_link['url'])
            os.remove(file.name)

        path = f'downloads{os.sep}{user_id}{os.sep}'
        file_name = file_path.split(os.sep)[-1]
        file_name = file_name.split('.')[0] + '.json'
        file = os.path.join(path, file_name)
        file_data = open(file, 'w')
        file_data.write(json.dumps(data, indent=2))
        file_data.close()
        self.bot.send_document(chat_id, open(file, 'rb'), **args)
        os.remove(file)
