import datetime
from typing import Optional

from anime_api.base_api import AnimeDetails


def start_anime(proxy: Optional[dict] = None) -> AnimeDetails:
    while True:
        anime_api = AnimeDetails()
        if proxy is not None:
            anime_api.cloud_scraper.proxies = proxy
        date = datetime.datetime.now().strftime('%Y-%m-%d %I:%M%p')
        print(f'[{date}] Trying to connect to {anime_api.base_url}')
        response = anime_api.cloud_scraper.get(anime_api.base_url)
        if response.status_code == 200:
            print(f'[{date}] Connection established.')
            break
    return anime_api
