import datetime
import sys

import sqlalchemy as db
from telegram import Message

from query import db_queue, server_log


class BotLogger:
    def __init__(self):
        self.format = '%Y-%m-%d %I:%M%p'
        self.date_now = datetime.datetime.now

    def save(self, **kwargs):
        query = db.insert(server_log)
        query = query.values(**kwargs)
        db_queue.put(query)

    def command_log(self, message: Message):
        username = message.from_user.name
        date = self.date_now().strftime(self.format)
        text = f'[{date}] {username}issued the'
        text += f' command "{message.text}"'
        print(text)
        self.save(
            command=message.text,
            account_id=message.from_user.id
        )

    def error_log(self):
        exc_type, exc_object, exc_traceback = sys.exc_info()
        number = exc_traceback.tb_lineno
        filename = exc_traceback.tb_frame.f_code.co_filename
        date = self.date_now().strftime(self.format)
        text = f'[{date}] Exception type "{exc_type}" on line'
        text += f' #{number} in {filename}: {exc_object}'
        print(text)
