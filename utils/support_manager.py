import telebot
from telegram import Message

from utils.command_manager import get_commands
from utils.sticker_manager import send_sticker


class HelpCommand:
    def __init__(self, bot: telebot.TeleBot, lang: dict):
        self.bot = bot
        self.lang = lang

    def start_tutorial(self, message: Message):
        lang = self.lang["help-text"]
        chat_id = message.chat.id
        message_id = message.message_id
        send_sticker(self.bot, chat_id, "Open door", message_id)
        text = lang["tutorial"].format(bot_name=self.bot.get_me().first_name)
        self.bot.send_message(chat_id, text)
        self.bot.send_message(chat_id, lang["about"])
        send_sticker(self.bot, chat_id, "Eating")
        self.features(message)

    def features(self, message: Message):
        lang = self.lang["help-text"]
        chat_id = message.chat.id
        self.bot.send_message(chat_id, lang["features"])
        self.bot.send_message(chat_id, lang["moodle-info"])
        send_sticker(self.bot, chat_id, "Reading")
        self.bot.send_message(chat_id, self.list_commands())

    def list_commands(self):
        command_list = get_commands()
        text = 'Use /help para mostrar el uso de comandos:\n\n'
        for command in command_list:
            text += f'» /{command.command}\n {command.description}\n\n'
        return text
