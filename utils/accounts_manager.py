import sqlalchemy as db
import telebot
from telegram import Message

from query import connection, profile, chats
from utils.sticker_manager import send_sticker


def print_profile(account: profile):
    name = account.first_name
    if account.last_name is not None:
        name += f' {account.last_name}'
    if account.username is not None:
        name += f' ({account.username})'
    text = f'{name}\nID: {account.id}\nPermisos: '
    text += 'Administrador\n' if account.is_admin else 'Usuario\n'
    text += f'Realizando alguna tarea: {account.is_locked}'
    return text


class AccountManager:
    def __init__(self, bot: telebot.TeleBot, lang: dict):
        self.bot = bot
        self.lang = lang

    def find_account(self, message: Message):
        try:
            first_name = profile.columns.first_name
            last_name = profile.columns.last_name
            username = profile.columns.username
            key = message.text.split(' ')[1]
            query = db.select([profile]).filter(db.or_(
                first_name == key, last_name == key, username == key
            ))
            accounts = [self.bot.send_message(message.chat.id, print_profile(account))
                        for account in connection.execute(query).fetchall()]
            if len(accounts) == 0:
                send_sticker(self.bot, message.chat.id, "Lost")
                self.bot.send_message(message.chat.id, self.lang["no-result"])
        except:
            send_sticker(self.bot, message.chat.id, "", reply_to=message)
            self.bot.send_message(message.chat.id, self.lang["command-not-allowed"])

    def wake_up(self):
        for chat in connection.execute(db.select([chats])).fetchall():
            try:
                send_sticker(self.bot, chat.id, "Meow")
                bot_name = self.bot.get_me().first_name
                text = f'La adorable {bot_name} esta de vuelta... Estas '
                text += 'haciendo algo? Me aburro y quiero ayudar en algo.'
                self.bot.send_message(chat.id, text)
            except Exception as error:
                print(error)
