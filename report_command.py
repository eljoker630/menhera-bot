import sqlalchemy as db
import telebot
from telegram import Message

from query import db_queue, report


class ReportError:
    def __init__(self, bot: telebot.TeleBot, lang):
        self.bot = bot
        self.lang = lang
        self.data = {}

    def report(self, message):
        chat_id = message.chat.id
        reply = self.bot.send_message(chat_id, self.lang["error-name"])
        self.bot.register_next_step_handler(reply, self.get_title)

    def get_title(self, message):
        chat_id = message.chat.id
        if len(message.text) < 10:
            text = self.lang["error-title-too-short"]
            reply = self.bot.send_message(chat_id, text)
            callback = self.get_title
        elif len(message.text) > 60:
            text = self.lang["error-title-too-long"]
            reply = self.bot.send_message(chat_id, text)
            callback = self.get_title
        else:
            text = self.lang["error-text"]
            reply = self.bot.send_message(chat_id, text)
            callback = self.get_text
            self.data['title'] = message.text
        self.bot.register_next_step_handler(reply, callback)

    def get_text(self, message):
        chat_id = message.chat.id
        if len(message.text) < 30:
            text = self.lang["error-text-too-short"]
            reply = self.bot.send_message(chat_id, text)
            callback = self.get_text
            self.bot.register_next_step_handler(reply, callback)
        else:
            text = self.lang["error-saved"]
            self.bot.send_message(chat_id, text)
            self.data['description'] = message.text
            self.save_data(message)

    def save_data(self, message: Message):
        self.data['account_id'] = message.from_user.id
        query = db.insert(report).values(**self.data)
        db_queue.put(query)
