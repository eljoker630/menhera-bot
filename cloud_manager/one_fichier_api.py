import re

import requests
from bs4 import BeautifulSoup


class OneFichier:
    def __init__(self, url):
        self.request = requests.session()
        self.main_url = url

    def get_details(self):
        response = self.request.get(self.main_url).content
        soup = BeautifulSoup(response, 'html.parser')
        table = soup.find('table', {'class': 'premium'})
        table = table.find_all('td')
        data = {
            'qr-code': table[0].find('img')['src'],
            'file-name': table[2].string,
            'creation-date': table[4].string,
            'file-size': table[6].string,
        }
        url_file = self.get_url(soup)
        is_number = str(type(url_file)) != "<class 'int'>"
        key = 'url-file' if is_number else 'cooldown'
        data[key] = url_file
        return data

    def get_url(self, soup: BeautifulSoup):
        form = soup.find('form', {'class': 'alc'})
        form_data = {}
        for data in form.find_all('input'):
            if data.has_key('name'):
                has_value = data.has_key('value')
                value = data['value'] if has_value else None
                form_data[data['name']] = value
        response = self.request.post(self.main_url, json=form_data)
        soup = BeautifulSoup(response.content, 'html.parser')
        button = soup.find('a', {'class': ['ok', 'btn-general', 'btn-orange']})
        if button is None:
            warning = soup.find_all('div', {'class': 'ct_warn'})[-1]
            cooldown = re.search(r'(?P<id>[0-9]+)', warning.decode())[0]
            return int(cooldown)
        return button['href']
