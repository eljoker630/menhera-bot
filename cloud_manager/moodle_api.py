import uuid

import requests
from bs4 import BeautifulSoup
from requests_toolbelt import MultipartEncoder


def get_parameters(url):
    params = {}
    for parameter in url.replace(url[:url.index('?')+1], '').split('&'):
        parameter = parameter.split('=')
        params[parameter[0]] = parameter[1]
    return params


class MoodleScraper:
    def __init__(self, url):
        self.moodle = url
        self.response = requests.Session()
        self.response.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux \
            x86_64; rv:97.0) Gecko/20100101 Firefox/97.0'
        }
        self.license = [
            'unknown', 'allrightsreserved', 'public', 'cc',
            'cc-nd', 'cc-nc-nd', 'cc-nc', 'cc-nc-sa', 'cc-sa'
        ]
        self.credentials = {}

    def authenticate(self, username, password):
        url = f'{self.moodle}login/index.php'
        response = self.response.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        token = soup.find('input', attrs={'name': 'logintoken'})['value']
        form_data = {
            'username': username, 'password': password,
            'anchor': '', 'logintoken': token
        }
        response = self.response.post(url, data=form_data)
        soup = BeautifulSoup(response.content, 'html.parser')
        login_info = soup.find_all('div', {'class': 'logininfo'})[1]

        profile_url = login_info.find('a')
        url = f'{self.moodle}user/profile.php?id='
        self.credentials['id'] = profile_url['href'].replace(url, '')
        self.credentials['name'] = profile_url.text

        url = f'{self.moodle}login/logout.php?sesskey='
        url_list = soup.find('div', {'id': 'carousel-item-main'})
        logout_url = url_list.find_all('a')[-1]['href']
        self.credentials['session-key'] = logout_url.replace(url, '')
        self.credentials['status'] = login_info.text
        return self.credentials

    def upload_file(self, file, license_id=1):
        url = f'{self.moodle}user/files.php'
        response = self.response.get(url).content
        soup = BeautifulSoup(response, 'html.parser')
        url = soup.find('object', attrs={'type': 'text/html'})['data']
        query = get_parameters(url)
        b = uuid.uuid4().hex
        upload_file = {
            'repo_upload_file': (file.name, file, 'application/octet-stream'),
            'title': (None, ''),
            'author': (None, self.credentials['name']),
            'license': (None, self.license[license_id]),
            'itemid': (None, query['itemid']),
            'repo_id': (None, str(self.credentials['repo-id'])),
            'p': (None, ''),
            'page': (None, ''),
            'env': (None, query['env']),
            'sesskey': (None, self.credentials['session-key']),
            'client_id': (None, self.credentials['id']),
            'maxbytes': (None, query['maxbytes']),
            'areamaxbytes': (None, query['areamaxbytes']),
            'ctx_id': (None, query['ctx_id']),
            'savepath': (None, '/')
        }
        url = f'{self.moodle}repository/repository_ajax.php?action=upload'
        multipart = MultipartEncoder(upload_file, boundary=b)
        header = self.response.headers
        header["Content-Type"] = f'multipart/form-data; boundary={b}'
        response = self.response.post(url, data=multipart, headers=header)
        return response.json()


# moodle = MoodleScraper('https://cursad.jovenclub.cu/')
# moodle.response.proxies = {
#     'http': 'http://192.168.43.1:8080',
#     'https': 'http://192.168.43.1:8080'
# }
# moodle.authenticate('angelohh', 'Siboney$292004')
# moodle.credentials['repo-id'] = 3
# print(moodle.credentials)
