import sqlalchemy as db
import telebot
from telegram import Message

from query import moodle, db_queue, connection
from utils.sticker_manager import send_sticker


class MoodleCommand:
    def __init__(self, bot: telebot.TeleBot, lang: dict):
        self.bot = bot
        self.lang = lang
        self.args = {}

    def get_moodle(self, account_id: int):
        column = moodle.columns.account_id
        query = db.select([moodle]).where(column == account_id)
        consult = connection.execute(query).fetchall()
        return consult[-1] if len(consult) != 0 else None

    def select_moodle(self, message: Message):
        account_id = message.from_user.id
        chat_id = message.chat.id
        moodle_server = self.get_moodle(account_id)
        if moodle_server is not None:
            value = not moodle_server.selected
            url = moodle_server.url
            column = moodle.columns.account_id
            query = db.update(moodle).values(selected=value)
            query = query.where(column == account_id)
            db_queue.put(query)
            if value:
                text = f'De ahora en adelante voy a subir la'
                text += f' multimedia al sitio web "{url}".'
            else:
                text = 'Los archivos los voy a enviar a partir'
                text += ' de ahora usando Telegram.'
            self.bot.send_message(chat_id, text)
        else:
            text = 'No hay ningun servidor moodle creado'
            text += ' con tu cuenta'
            self.bot.send_message(chat_id, text)

    def start_form(self, message: Message):
        chat_id = message.chat.id
        if self.get_moodle(chat_id) is None:
            self.args['account_id'] = message.from_user.id
            text = self.lang["moodle-host"]
            reply = self.bot.send_message(chat_id, text)
            callback = self.set_host
            self.bot.register_next_step_handler(reply, callback)
        else:
            text = 'Ya existe un moodle asociado a su cuenta.'
            self.bot.send_message(chat_id, text)

    def set_host(self, message: Message):
        url = message.text
        self.args['url'] = url + '/' if url[-1] != '/' else url
        text = self.lang["moodle-username"]
        reply = self.bot.send_message(message.chat.id, text)
        callback = self.get_username
        self.bot.register_next_step_handler(reply, callback)

    def get_username(self, message: Message):
        self.args['username'] = message.text
        text = self.lang["moodle-password"]
        chat_id = message.chat.id
        reply = self.bot.send_message(chat_id, text)
        callback = self.get_password
        self.bot.register_next_step_handler(reply, callback)

    def get_password(self, message: Message):
        self.args['password'] = message.text
        text = self.lang["moodle-repo-id"]
        chat_id = message.chat.id
        reply = self.bot.send_message(chat_id, text)
        callback = self.check_credentials
        self.bot.register_next_step_handler(reply, callback)

    def check_credentials(self, message: Message):
        self.args['repo_id'] = int(message.text)
        text = self.lang["moodle-credentials"]
        chat_id = message.chat.id
        button = telebot.types.KeyboardButton
        markup = telebot.types.ReplyKeyboardMarkup
        markup = markup(one_time_keyboard=True, resize_keyboard=True)
        markup.add(*[button('Aceptar'), button('Editar'), button('Cancela')])
        reply = self.bot.send_message(chat_id, text, reply_markup=markup)
        callback = self.select_option
        self.bot.register_next_step_handler(reply, callback)

    def select_option(self, message: Message):
        markup = {'reply_markup': telebot.types.ReplyKeyboardRemove()}
        chat_id = message.chat.id
        send_sticker(self.bot, chat_id, "OK!", **markup)
        if message.text == 'Aceptar':
            query = db.insert(moodle).values(**self.args)
            db_queue.put(query)
        elif message.text == 'Editar': self.start_form(message)
