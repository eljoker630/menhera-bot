import json
import os
import threading
from sys import argv

import sqlalchemy as db
import telebot
from telebot import apihelper
from telebot.types import Message

from anime_api.chapter_command import ChapterCommands
from cloud_manager.moodle_command import MoodleCommand
from magas_api.mangas_command import MangaCommands
from manhwas_api.manhwas_command import ManhwaCommands
from query import profile, sqlite_worker, lock_message, is_admin, db_queue, connection, chats, engine, metadata
from report_command import ReportError
from utils.accounts_manager import AccountManager
from utils.command_manager import get_commands
from utils.file_manager import UploadManager
from utils.scraper_starter import start_anime
from utils.sticker_manager import send_sticker
from anime_api.anime_command import AnimeCommands
from utils.support_manager import HelpCommand

bot = telebot.TeleBot("5156066838:AAF1q47nGS93N8zuxB_HldhBaP9A_nbzw74")
secret_key = '0qlH6yL9hzw1JF2vnmbIMS65k9tJ9raVN2tdfTWOKiY='
lang = json.loads(open('language/es.json', 'rb').read())
proxy = argv[argv.index('-p') + 1] if '-p' in argv else None
apihelper.proxy = {'http': proxy, 'https': proxy} if proxy else None
anime_api = start_anime(apihelper.proxy)
metadata.create_all(engine)
command_list = get_commands()
bot.set_my_commands(commands=command_list)
threading.Thread(target=sqlite_worker, daemon=True).start()
account_manager = AccountManager(bot, lang)
account_manager.wake_up() if '--dev' not in argv else None


@bot.message_handler(func=lambda m: lock_message(m))
def print_locked(message: Message):
    send_sticker(bot, message.chat.id, "AAaAaAaa")
    bot.send_message(message.chat.id, lang["lock-message"])


@bot.message_handler(commands=['report'])
def report_command(message: Message):
    """Encontrastes algun bug? este comando es el indicado para esas tareas."""
    report = ReportError(bot, lang)
    report.report(message)


@bot.message_handler(commands=['help'])
def help_command(message: Message):
    HelpCommand(bot, lang).features(message)


@bot.message_handler(commands=['start'])
def start_bot(message: Message):
    HelpCommand(bot, lang).start_tutorial(message)


# Anime Commands
@bot.message_handler(commands=['chapter_today'])
def chapter_today(message: Message):
    """Muestra el listado de capitulos del dia de los animes en transmision."""
    chapter_commands = ChapterCommands(bot, lang, anime_api)
    chapter_commands.start_command(message)


@bot.message_handler(commands=['anime_today'])
def anime_today(message: Message):
    """Muestra el listado de animes en transmision."""
    commands = AnimeCommands(bot, lang, anime_api)
    commands.anime_today(message)


@bot.message_handler(commands=['anime_search'])
def anime_search(message: Message):
    """Busca un anime con este comando."""
    commands = AnimeCommands(bot, lang, anime_api)
    commands.anime_search(message)


# Manhwa commands
@bot.message_handler(commands=['manhwa_updates'])
def latest_chapter(message: Message):
    """Muestra los ultimos capitulos agregados de cada manhwa."""
    manhwa = ManhwaCommands(bot, lang)
    proxies = anime_api.cloud_scraper.proxies
    manhwa.manhwa_api.request.proxies = proxies
    manhwa.last_chapter(message)


@bot.message_handler(commands=['manhwa_latest'])
def latest_manhwa(message: Message):
    """Aqui tienes el listado de los ultimos manhwas agregados."""
    manhwa = ManhwaCommands(bot, lang)
    proxies = anime_api.cloud_scraper.proxies
    manhwa.manhwa_api.request.proxies = proxies
    manhwa.latest_manhwa(message)


@bot.message_handler(commands=['manhwa_search'])
def search_manhwa(message: Message):
    """Busca entre la aplia coleccion de manhwas existente."""
    manhwa = ManhwaCommands(bot, lang)
    proxies = anime_api.cloud_scraper.proxies
    manhwa.manhwa_api.request.proxies = proxies
    manhwa.find_manhwa(message)


# Manga Commands
@bot.message_handler(commands=['manga_today'])
def manga_today(message: Message):
    """Muestra los mangas del día."""
    manga = MangaCommands(bot, lang)
    proxies = anime_api.cloud_scraper.proxies
    manga.manga_api.request.proxies = proxies
    manga.manga_today(message)


@bot.message_handler(commands=['manga_top'])
def manga_top(message: Message):
    """Muestra el listado de mangas que son recomendados por la comunidad."""
    manga = MangaCommands(bot, lang)
    proxies = anime_api.cloud_scraper.proxies
    manga.manga_api.request.proxies = proxies
    manga.manga_top(message)


@bot.message_handler(commands=['manga_search'])
def manga_search(message: Message):
    """Realiza una busqueda de los mangas utilizando este comando."""
    manga = MangaCommands(bot, lang)
    proxies = anime_api.cloud_scraper.proxies
    manga.manga_api.request.proxies = proxies
    manga.manga_search(message)


@bot.message_handler(commands=['add_moodle'])
def add_moodle(message: Message):
    moodle = MoodleCommand(bot, lang)
    moodle.start_form(message)


@bot.message_handler(commands=['toggle_moodle'])
def set_moodle(message: Message):
    moodle = MoodleCommand(bot, lang)
    moodle.select_moodle(message)


@bot.message_handler(commands=['wget'])
def download_url(message: Message):
    """Descarga archivos de enlaces directos."""
    send_sticker(bot, message.chat.id, "Hom hom")
    manager = UploadManager(bot, lang)
    manager.proxy = anime_api.cloud_scraper.proxies
    manager.wget_upload(message)


@bot.message_handler(commands=['zippy'])
def zippy_download(message: Message):
    """Descarga links de Zippy Share."""
    send_sticker(bot, message.chat.id, "Hom hom")
    manager = UploadManager(bot, lang)
    manager.proxy = anime_api.cloud_scraper.proxies
    manager.zippy_upload(message)


@bot.message_handler(commands=['mega'])
def mega_upload(message: Message):
    """Descarga archivos de mega usando este comando."""
    send_sticker(bot, message.chat.id, "Hom hom")
    manager = UploadManager(bot, lang)
    manager.proxy = anime_api.cloud_scraper.proxies
    manager.mega_upload(message)


@bot.message_handler(commands=['1fichier'])
def one_fichier(message: Message):
    """Descarga de 1fichier usando este comando."""
    send_sticker(bot, message.chat.id, "Hom hom")
    manager = UploadManager(bot, lang)
    manager.proxy = anime_api.cloud_scraper.proxies
    manager.one_fichier_upload(message)


# Admin commands
@bot.message_handler(commands=['set_admin'])
def set_admin(message: Message):
    try:
        client_key = message.text.split(' ')[1]
        account_id = message.from_user.id
        text = lang["key-error"]
        if client_key == secret_key:
            query = db.update(profile).values(is_admin=True)
            query = query.where(profile.columns.id == account_id)
            db_queue.put(query)
            text = lang["key-success"]
    except:
        text = lang["key-none"]
    bot.send_message(message.chat.id, text)


@bot.message_handler(func=lambda m: is_admin(m), commands=['unlock'])
def unlock_account(message: Message):
    """Establece en falso el bloqueo del usuario seleccionado."""
    command = message.text.split(' ')
    query = db.update(profile).values(is_locked=False)
    account_id = message.from_user.id
    account_id = account_id if len(command) != 2 else command[1]
    query = query.where(profile.columns.id == account_id)
    db_queue.put(query)
    text = lang["forgot-unlock"]
    reply = bot.send_message(message.chat.id, text)
    send_sticker(bot, message.chat.id, "Kyu", reply)


@bot.message_handler(func=lambda m: is_admin(m), commands=['find_account'])
def find_account(message: Message):
    account_manager.find_account(message)


@bot.message_handler(func=lambda m: is_admin(m), commands=['stop'])
def stop_bot(message: Message):
    """Desconecta a Menhera."""
    query = connection.execute(db.select([chats])).fetchall()
    text = 'De repente tengo mucho sueño, supongo que dormire '
    text += ' un rato me despiertas si haces galletitas, vale?'
    for chat in query:
        try:
            bot.send_message(chat.id, text)
            send_sticker(bot, chat.id, "Good Night")
        except: pass
    bot.stop_bot()


# 404 Commands
@bot.message_handler(func=lambda message: True, content_types=['text'])
def command_default(message: Message):
    send_sticker(bot, message.chat.id, "Question", message.message_id)
    text = lang["command-notfound"].format(command=message.text)
    bot.send_message(message.chat.id, text)


@bot.message_handler(func=lambda message: True, content_types=['sticker'])
def command_test(message: Message):
    bot.send_message(message.chat.id, lang["sticker-received"])
    bot.send_message(message.chat.id, message.sticker.file_id)


bot.infinity_polling()
