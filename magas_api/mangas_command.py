import os
import shutil

import telebot
from telegram import Message

from anime_api.serializer import keyboard_chapter
from magas_api.serializer import reformat_manga, print_manga
from magas_api.inmanga_api import InManga
from split_compress import zip_file
from utils.file_manager import UploadManager
from utils.sticker_manager import send_sticker


class MangaCommands:
    def __init__(self, bot: telebot.TeleBot, lang: dict):
        self.markup = telebot.types.ReplyKeyboardMarkup()
        self.bot, self.lang = bot, lang
        self.manga_api = InManga()
        self.chapters, self.mangas = [], {}

    def manga_today(self, message: Message, show_chapters=True):
        self.chapters = self.manga_api.recent_chapters()
        text = reformat_manga(self.chapters, self.lang)
        text = f"{text['text']}\n{text['action-text']}"
        if not show_chapters: text = text.split('\n')[-1]
        next_step = self.bot.register_next_step_handler
        length = range(len(self.chapters))
        episodes = ['%02d' % (i + 1) for i in length][0:299]
        reply = self.print_keyboard(message, episodes, text)
        next_step(reply, self.select_chapter)

    def manga_search(self, message: Message):
        text = message.text.replace('/manga_search ', '')
        result = self.manga_api.search(100, text)
        self.print_mangas(result, message.chat.id)

    def manga_top(self, message: Message):
        mangas = self.manga_api.most_viewed_mangas()
        self.print_mangas(mangas, message.chat.id)

    def print_mangas(self, mangas: list, chat_id: int):
        for manga in mangas:
            try:
                args = self.manga_api, manga, self.lang
                data = print_manga(*args)
                args = chat_id, data['image'], data['text']
                self.mangas[data['id']] = data
                self.bot.send_photo(*args)
            except: pass
        args = chat_id, self.lang["task-terminated"]
        args = self.bot.send_message(*args), self.select_manga
        self.bot.register_next_step_handler(*args)

    def print_keyboard(self, message: Message, episodes, text):
        command, chat_id = ['Cancelar'], message.chat.id
        self.markup = keyboard_chapter(episodes, command)
        send_message, args = self.bot.send_message, (chat_id, text)
        return send_message(*args, reply_markup=self.markup)

    def select_manga(self, message: Message):
        if 'cancel' in message.text.lower():
            markup = telebot.types.ReplyKeyboardRemove()
            arguments = self.bot, message.chat.id, 'Good'
            send_sticker(*arguments, reply_markup=markup)
            return 0
        manga_id = int(message.text.replace('/manga_', ''))
        select = lambda m: m == manga_id
        manga_id = next(m for m in self.mangas if select(m))
        self.chapters = self.mangas[manga_id]['chapters']
        episodes = [c["Number"] for c in self.chapters][:299]
        episodes.sort()
        args = message, episodes, self.lang["select-chapter"]
        args = self.print_keyboard(*args), self.select_chapter
        self.bot.register_next_step_handler(*args, in_manga=True)

    def select_chapter(self, message: Message, in_manga=False):
        markup = telebot.types.ReplyKeyboardRemove()
        arguments = self.bot, message.chat.id, 'Roger'
        send_sticker(*arguments, reply_markup=markup)
        if 'cancel' in message.text.lower(): return 0
        if not in_manga:
            chapter = self.chapters[int(message.text) - 1]
            uuid = chapter['chapter-url'].split('/')[-1]
        else:
            chapter_id = float(message.text)
            select = lambda c: c['Number'] == chapter_id
            chapter = next(c for c in self.chapters if select(c))
            uuid = chapter['Identification']
        chapter = self.manga_api.get_chapter(uuid)
        self.download_chapter(message, chapter)
        callback = self.select_chapter
        reply = self.cache_keyboard(message)
        next_step = self.bot.register_next_step_handler
        next_step(reply, callback, in_manga=in_manga)

    def cache_keyboard(self, message):
        args = message.chat.id, self.lang["task-terminated"]
        send_message = self.bot.send_message
        return send_message(*args, reply_markup=self.markup)

    def download_chapter(self, message: Message, chapter):
        manga = chapter['manga-url'].split('/')[-2]
        path = f'downloads/{message.chat.id}/{manga}'
        path, images = path.replace('/', os.sep), []
        os.makedirs(path) if not os.path.isdir(path) else None
        for index, photo in enumerate(chapter['images']):
            data = self.manga_api.request.get(photo)
            extension = data.headers['Content-Type'].split('/')[-1]
            images.append(f'{path}/{index}.{extension}')
            images[-1] = images[-1].replace('/', os.sep)
            open(images[-1], 'wb').write(data.content)
        zipped = zip_file(images, path + '.zip')
        shutil.rmtree(path)
        manager = UploadManager(self.bot, self.lang)
        manager.proxy = self.manga_api.request.proxies
        manager.upload_file(message, zipped.name)
