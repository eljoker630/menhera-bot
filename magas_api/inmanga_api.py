import json
import re

import requests
from bs4 import BeautifulSoup


class InManga:
    def __init__(self):
        self.request = requests.Session()
        self.url = 'https://inmanga.com'
        self.only_numbers = lambda text: int(re.sub(r'[^0-9]+', '', text))

    def recent_chapters(self) -> list:
        url = self.url + '/chapter/getRecentChapters'
        response = self.request.get(url).content
        item_list = [{
            'image': item.find('img')['src'],
            'chapter-url': self.url + item['href'],
            'chapter-name': item.find('strong').text.replace('\n', '').strip(),
            'chapter-number': self.only_numbers(item.find_all('strong')[1].text)
        } for item in BeautifulSoup(response, 'html.parser').find_all('a')]
        return [i for n, i in enumerate(item_list) if i not in item_list[:n]]

    def most_viewed_chapters(self):
        url = self.url + '/chapter/getMostViewedChapters'
        response = self.request.get(url).content
        return [{
            'image': item.find('img')['src'],
            'chapter-url': self.url + item['href'],
            'chapter-name': item.find('strong').text.replace('\n', '').strip(),
            'chapter-number': self.only_numbers(item.find_all('strong')[1].text),
            'views': self.only_numbers(item.find_all('span')[2].text)
        } for item in BeautifulSoup(response, 'html.parser').find_all('a')]

    def most_viewed_mangas(self) -> list:
        url = self.url + '/manga/getMostViewedMangas'
        response = self.request.get(url).content
        return [{
            'image': item.find('img')['src'],
            'manga-url': self.url + item['href'],
            'manga-name': item.find('strong').text.replace('\n', '').strip(),
            'views': self.only_numbers(item.find_all('span')[1].text)
        } for item in BeautifulSoup(response, 'html.parser').find_all('a')]

    def get_chapter(self, chapter_uuid):
        url = f'{self.url}/chapter/chapterIndexControls'
        url = f'{url}?identification={chapter_uuid}'
        response = self.request.get(url).content
        soup = BeautifulSoup(response, 'html.parser')

        chapter = soup.find('input', {'id': 'FriendlyChapterNumberUrl'})
        chapter = chapter['value']
        manga = soup.find('input', {'id': 'FriendlyMangaName'})['value']

        select = soup.find('select', {'id': 'PageList'})
        url = f'https://pack-yak.intomanga.com/images/manga/'
        url += f'{manga}/chapter/{chapter}/page/'
        images = [f'{url}{option.string}/{option["value"]}'
                  for option in select.find_all('option')]
        manga = soup.find('a', {'class': 'blue'})['href']
        return {'manga-url': manga, 'images': images}

    def get_manga(self, url) -> dict:
        response = self.request.get(url).content
        soup = BeautifulSoup(response, 'html.parser')
        synopsis = soup.find('div', {'class': 'panel-body'}).text
        url_chapter = f"{self.url}/chapter/getall?mangaIdentification={url.split('/')[-1]}"
        categories = soup.find_all('div', {'class': 'panel-heading'})[1].find_all('span')
        categories = [category.text.replace('\n', '').strip() for category in categories]
        data = {
            'title': soup.find('div', {'class': 'panel-heading'}).text,
            'image': self.url + soup.find('div', {'class': 'panel'}).find('img')['src'],
            'categories': categories,
            'synopsis': synopsis.replace('\n', '').strip(),
            'chapters': json.loads(self.request.get(url_chapter).json()['data'])['result']
        }
        list_group = soup.find('div', {'class': 'list-group'})
        for a in list_group.find_all('a'):
            value = a.find('span').string
            field = a.text.replace('\n', '')
            if value is not None:
                field = field.replace(value, '')
            data[field.strip().lower()] = value
        return data

    def search(self, paginate: int, manga=None, skip=None):
        manga = '' if manga is None else manga
        skip = 0 if skip is None else skip
        data = {
            'hfilter[generes][]': '-1',
            'filter[queryString]': manga,
            'filter[skip]': skip,
            'filter[take]': paginate,
            'filter[sortby]': '1',
            'filter[broadcastStatus]': '0',
            'filter[onlyFavorites]': 'false'
        }
        url = self.url + '/manga/getMangasConsultResult'
        request = self.request.post(url, data=data)
        soup = BeautifulSoup(request.content, 'html.parser')
        result_list = []
        for a in soup.find_all('a'):
            data = [data.text.replace('\n', '') for data in a.find_all('h4')[1:]]
            result_list.append({
                'manga-url': self.url + a['href'],
                'name': a.find('h4').text.strip(),
                'image': self.url + a.find('img')['data-src'],
                'status': data[0],
                'latest-publication': data[1],
                'frequency': data[2],
                'chapters': self.only_numbers(data[3]),
            })
        return result_list
