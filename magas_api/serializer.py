from anime_api.serializer import reformat_chapters
from magas_api.inmanga_api import InManga


def reformat_manga(chapters: list, lang: dict):
    chapters = [{
        'title': c['chapter-name'],
        'chapter': f'Episodio {c["chapter-number"]}'
    } for c in chapters]
    return reformat_chapters(chapters, lang)


def print_manga(api: InManga, manga: dict, lang):
    manga = api.get_manga(manga['manga-url'])
    category = ', '.join(manga['categories'])
    chapters = len(manga['chapters'])
    synopsis = f"{manga['synopsis'][:290]}..."
    data = list(manga.keys())[5:]
    command = f'manga_{manga["chapters"][0]["MangaId"]}'
    data = '\n'.join(f'{k}: {manga[k]}' for k in data)
    format_text = {'command': command, 'object': 'Manga'}
    data = '\n'.join([
        manga['title'], f'Capitulos {chapters}', data,
        f'Categoria: {category}', f'Sinopsis: {synopsis}',
        f'\n{lang["select-object"].format(**format_text)}'
    ])
    manga['id'] = manga["chapters"][0]["MangaId"]
    return {'text': data, **manga}
