import datetime
import os
import queue

import sqlalchemy as db
from telegram import Message

database_url = 'sqlite:///database.sqlite'
engine = db.create_engine(database_url, connect_args={'check_same_thread': False})
metadata = db.MetaData()
db_queue = queue.Queue()
connection = engine.connect()

chats = db.Table(
    'chat', metadata,
    db.Column('id', db.Integer, primary_key=True),
    db.Column('title', db.String),
    db.Column('description', db.String),
    db.Column('invite_link', db.String),
    db.Column('type', db.String)
)

profile = db.Table(
    'profile', metadata,
    db.Column('id', db.Integer, primary_key=True),
    db.Column('first_name', db.String),
    db.Column('last_name', db.String),
    db.Column('username', db.String),
    db.Column('is_bot', db.Boolean, default=False),
    db.Column('is_locked', db.Boolean, default=False),
    db.Column('is_admin', db.Boolean, default=False),
    db.Column('phone', db.String(length=8), nullable=True)
)

client_chat = db.Table(
    'relation_chat', metadata,
    db.Column('id', db.Integer, primary_key=True),
    db.Column('chat_id', db.Integer),
    db.Column('profile_id', db.Integer),
)

moodle = db.Table(
    'moodle', metadata,
    db.Column('id', db.Integer, primary_key=True, autoincrement=True),
    db.Column('account_id', db.Integer, unique=True),
    db.Column('username', db.String),
    db.Column('password', db.String),
    db.Column('url', db.String),
    db.Column('repo_id', db.Integer),
    db.Column('selected', db.Boolean),
)

report = db.Table(
    'report', metadata,
    db.Column('id', db.Integer, primary_key=True, autoincrement=True),
    db.Column('account_id', db.Integer),
    db.Column('title', db.String),
    db.Column('description', db.String),
    db.Column('date', db.DateTime, default=datetime.datetime.now)
)

server_log = db.Table(
    'server_log', metadata,
    db.Column('id', db.Integer, primary_key=True, autoincrement=True),
    db.Column('account_id', db.Integer),
    db.Column('command', db.String),
    db.Column('date', db.DateTime, default=datetime.datetime.now)
)


def sqlite_worker():
    while True:
        query = db_queue.get()
        connection.execute(query)


def set_chat(message: Message):
    column = chats.columns.id
    chat_id = message.chat.id
    query = db.select([chats]).where(column == chat_id)
    consult = connection.execute(query).fetchall()
    if len(consult) == 0:
        db_queue.put(db.insert(chats).values(
            id=message.chat.id,
            title=message.chat.title,
            description=message.chat.description,
            invite_link=message.chat.invite_link,
            type=message.chat.type
        ))
    db_queue.put(db.insert(client_chat).values(
        chat_id=message.chat.id,
        profile_id=message.from_user.id
    ))


def get_profile(message: Message):
    account_id = message.from_user.id
    column = profile.columns.id
    query = db.select([profile]).where(column == account_id)
    consult = connection.execute(query).fetchall()
    if len(consult) == 0:
        return None
    return consult[0]


def log_command(message: Message):
    date = datetime.datetime.now().strftime('%Y-%m-%d %I:%M%p')
    username = message.from_user.first_name
    formatted = f'[{date}] {username} issued the command "{message.text}"'
    query = db.insert(server_log).values(
        account_id=message.from_user.id,
        command=message.text
    )
    db_queue.put(query)
    print(formatted)


def lock_message(message: Message):
    log_command(message)
    account = get_profile(message)
    if account is None:
        path = os.path.join('downloads', str(message.from_user.id))
        os.makedirs(path) if os.path.isdir(path) else None
        query = db.insert(profile).values(
            id=message.from_user.id,
            first_name=message.from_user.first_name,
            last_name=message.from_user.last_name,
            username=message.from_user.username,
            is_bot=message.from_user.is_bot
        )
        db_queue.put(query)
        set_chat(message)
        return False
    return account.is_locked


def is_admin(message: Message):
    account = get_profile(message)
    return False if account is None else account.is_admin
